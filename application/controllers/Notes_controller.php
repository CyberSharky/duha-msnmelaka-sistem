<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notes_controller extends CI_Controller {

	//private $uid;
	public function __construct(){

		parent::__construct(); //must be added
        // check login first
        //$this->auth_model->check_login();
        $this->load->database();

        // assign general var
        //$this->uid = $this->session->userdata('user_id');
	}

	public function new_notes(){

		$errors         = array();  	// array to hold validation errors
		$data 			= array(); 		// array to pass back data

		//return response)
		$uid = $this->input->post('uid');
		$title = $this->input->post('title');
		$content = $this->input->post('content');

		$dbdata = array(
			'user_id' => $uid,
			'title' => $title,
			'content' => $content
		);

		$result = $this->db->insert('test_note', $dbdata);

		$data['success'] = true;

		echo json_encode($data);

	}

	public function getNotesByUserId()
	{

	}

	public function getNotesByNotesId()
	{
		
	}


	public function update_notes(){
		$errors         = array();  	// array to hold validation errors
		$data 			= array(); 		// array to pass back data

		//return response)
		$uid = $this->input->post('uid');
		$title = $this->input->post('title');
		$content = $this->input->post('content');

		$dbdata = array(
			'title' => $title,
			'content' => $content
		);

		$this->db->where('user_id', $uid);
		$result = $this->db->update('test_note', $dbdata);

		$data['success'] = true;

		echo json_encode($data);
	}
	
}
//end of file: Notes_controller.php