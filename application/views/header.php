<?php
	//set url for all links here

	//link pentadbiran
	$l_pahm = base_url()."pentadbiran/aset/hartamodel";
	$l_painv = base_url()."pentadbiran/aset/inventori";
	$l_paset = base_url()."pentadbiran/aset/asetahli";
	$l_psaset = base_url()."pentadbiran/aset/senaraiaset";
	$l_pkdaftar = base_url()."pentadbiran/kakitangan/pendaftaran";
	$l_pkcuti = base_url()."pentadbiran/kakitangan/rekodcuti";
	$l_pkrakam = base_url()."pentadbiran/kakitangan/perakamwaktu";
	$l_psmstok = base_url()."pentadbiran/stok/mohonstok";
	$l_psmsukan = base_url()."pentadbiran/stok/mohonalatsukan";
	//end: link pentadbiran

	//link sukma
	$l_sdatlet = base_url()."sukma/pendaftaran/atlet";
	$l_sdpasukan = base_url()."sukma/pendaftaran/pasukan";
	$l_sdjl = base_url()."sukma/pendaftaran/jurulatih";
	$l_slbank = base_url()."sukma/lampiran/bank";
	$l_sliic = base_url()."sukma/lampiran/iic";
	$l_statlet = base_url()."sukma/tawaran/atlet";
	$l_stjl = base_url()."sukma/tawaran/jurulatih";
	//end: link sukma

	//link usptn
	$l_ubio = base_url()."usptn/biodata";
	$l_ulatih = base_url()."usptn/pusatlatihan";
	$l_uhadir = base_url()."usptn/kehadiranatlet";
	$l_uatlet = base_url()."usptn/infoatlet";
	$l_ulatlet = base_url()."usptn/laporan/atlet";
	$l_uljl = base_url()."usptn/laporan/jurulatih";
	$l_ubmalat = base_url()."usptn/borang/mohonalat";
	$l_ubmakt = base_url()."usptn/borang/mohonaktiviti";
	$l_ut = base_url()."usptn/takwim";
	//end: usptn

	//link sistem
	$l_sislog = base_url()."sistem/log";
	$l_sisacc = base_url()."sistem/akaun";
	$l_sisdbbak = base_url()."sistem/db/backup";
	$l_sisdbres = base_url()."sistem/db/restore";
	//end: sistem
?>

<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.0 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- start: HEAD -->
	<head>
		<title>Rapido - Responsive Admin Template</title>
		<!-- start: META -->
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- end: META -->
		<!-- start: MAIN CSS -->
		<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?php echo base_url('inc/plugins/bootstrap/css/bootstrap.min.css');?>">
		<link rel="stylesheet" href="<?php echo base_url('inc/plugins/font-awesome/css/font-awesome.min.css');?>">
		<link rel="stylesheet" href="<?php echo base_url('inc/plugins/iCheck/skins/all.css');?>">
		<link rel="stylesheet" href="<?php echo base_url('inc/plugins/perfect-scrollbar/src/perfect-scrollbar.css');?>">
		<link rel="stylesheet" href="<?php echo base_url('inc/plugins/animate.css/animate.min.css');?>">
		<!-- end: MAIN CSS -->
		<!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->
		<link rel="stylesheet" href="<?php echo base_url('inc/plugins/owl-carousel/owl-carousel/owl.carousel.css');?>">
		<link rel="stylesheet" href="<?php echo base_url('inc/plugins/owl-carousel/owl-carousel/owl.theme.css');?>">
		<link rel="stylesheet" href="<?php echo base_url('inc/plugins/owl-carousel/owl-carousel/owl.transitions.css');?>">
		<link rel="stylesheet" href="<?php echo base_url('inc/plugins/summernote/dist/summernote.css');?>">
		<link rel="stylesheet" href="<?php echo base_url('inc/plugins/fullcalendar/fullcalendar/fullcalendar.css');?>">
		<link rel="stylesheet" href="<?php echo base_url('inc/plugins/toastr/toastr.min.css');?>">
		<link rel="stylesheet" href="<?php echo base_url('inc/plugins/bootstrap-select/bootstrap-select.min.css');?>">
		<link rel="stylesheet" href="<?php echo base_url('inc/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css');?>">
		<link rel="stylesheet" href="<?php echo base_url('inc/plugins/DataTables/media/css/DT_bootstrap.css');?>">
		<link rel="stylesheet" href="<?php echo base_url('inc/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css');?>">
		<link rel="stylesheet" href="<?php echo base_url('inc/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css');?>">
		<!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" href="<?php echo base_url('inc/plugins/weather-icons/css/weather-icons.min.css');?>">
		<link rel="stylesheet" href="<?php echo base_url('inc/plugins/nvd3/nv.d3.min.css');?>">
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
		<!-- start: CORE CSS -->
		<link rel="stylesheet" href="<?php echo base_url('inc/css/styles.css');?>">
		<link rel="stylesheet" href="<?php echo base_url('inc/css/styles-responsive.css');?>">
		<link rel="stylesheet" href="<?php echo base_url('inc/css/plugins.css');?>">
		<link rel="stylesheet" href="<?php echo base_url('inc/css/themes/theme-default.css');?>"  type="text/css" id="skin_color">
		<link rel="stylesheet" href="<?php echo base_url('inc/css/print.css');?>" type="text/css" media="print"/>
		<!-- end: CORE CSS -->
		<link rel="shortcut icon" href="favicon.ico" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<div id="slidingbar-area">
			<div id="slidingbar">
				<div class="row">
					<!-- start: SLIDING BAR FIRST COLUMN -->
					<div class="col-md-4 col-sm-4">
						<h2>My Options</h2>
						<div class="row">
							<div class="col-xs-6 col-lg-3">
								<button class="btn btn-icon btn-block space10">
									<i class="fa fa-folder-open-o"></i>
									Projects <span class="badge badge-info partition-red"> 4 </span>
								</button>
							</div>
							<div class="col-xs-6 col-lg-3">
								<button class="btn btn-icon btn-block space10">
									<i class="fa fa-envelope-o"></i>
									Messages <span class="badge badge-info partition-red"> 23 </span>
								</button>
							</div>
							<div class="col-xs-6 col-lg-3">
								<button class="btn btn-icon btn-block space10">
									<i class="fa fa-calendar-o"></i>
									Calendar <span class="badge badge-info partition-blue"> 5 </span>
								</button>
							</div>
							<div class="col-xs-6 col-lg-3">
								<button class="btn btn-icon btn-block space10">
									<i class="fa fa-bell-o"></i>
									Notifications <span class="badge badge-info partition-red"> 9 </span>
								</button>
							</div>
						</div>
					</div>
					<!-- end: SLIDING BAR FIRST COLUMN -->
					<!-- start: SLIDING BAR SECOND COLUMN -->
					<div class="col-md-4 col-sm-4">
						<h2>My Recent Works</h2>
						<div class="blog-photo-stream margin-bottom-30">
							<ul class="list-unstyled">
								<li>
									<a href="#"><img alt="" src="<?php echo base_url('inc/images/image01_th.jpg');?>"></a>
								</li>
								<li>
									<a href="#"><img alt="" src="<?php echo base_url('inc/images/image02_th.jpg');?>"></a>
								</li>
								<li>
									<a href="#"><img alt="" src="<?php echo base_url('inc/images/image03_th.jpg');?>"></a>
								</li>
								<li>
									<a href="#"><img alt="" src="<?php echo base_url('inc/images/image04_th.jpg');?>"></a>
								</li>
								<li>
									<a href="#"><img alt="" src="<?php echo base_url('inc/images/image05_th.jpg');?>"></a>
								</li>
								<li>
									<a href="#"><img alt="" src="<?php echo base_url('inc/images/image06_th.jpg');?>"></a>
								</li>
								<li>
									<a href="#"><img alt="" src="<?php echo base_url('inc/images/image07_th.jpg');?>"></a>
								</li>
								<li>
									<a href="#"><img alt="" src="<?php echo base_url('inc/images/image08_th.jpg');?>"></a>
								</li>
								<li>
									<a href="#"><img alt="" src="<?php echo base_url('inc/images/image09_th.jpg');?>"></a>
								</li>
								<li>
									<a href="#"><img alt="" src="<?php echo base_url('inc/images/image10_th.jpg');?>"></a>
								</li>
							</ul>
						</div>
					</div>
					<!-- end: SLIDING BAR SECOND COLUMN -->
					<!-- start: SLIDING BAR THIRD COLUMN -->
					<div class="col-md-4 col-sm-4">
						<h2>My Info</h2>
						<address class="margin-bottom-40">
							Peter Clark
							<br>
							12345 Street Name, City Name, United States
							<br>
							P: (641)-734-4763
							<br>
							Email:
							<a href="#">
								peter.clark@example.com
							</a>
						</address>
						<a class="btn btn-transparent-white" href="#">
							<i class="fa fa-pencil"></i> Edit
						</a>
					</div>
					<!-- end: SLIDING BAR THIRD COLUMN -->
				</div>
				<div class="row">
					<!-- start: SLIDING BAR TOGGLE BUTTON -->
					<div class="col-md-12 text-center">
						<a href="#" class="sb_toggle"><i class="fa fa-chevron-up"></i></a>
					</div>
					<!-- end: SLIDING BAR TOGGLE BUTTON -->
				</div>
			</div>
		</div>
		<!-- end: SLIDING BAR -->
		<div class="main-wrapper">
			<!-- start: TOPBAR -->
			<header class="topbar navbar navbar-inverse navbar-fixed-top inner">
				<!-- start: TOPBAR CONTAINER -->
				<div class="container">
					<div class="navbar-header">
						<a class="sb-toggle-left hidden-md hidden-lg" href="#main-navbar">
							<i class="fa fa-bars"></i>
						</a>
						<!-- start: LOGO -->
						<a class="navbar-brand" href="index.html">
							<img src="<?php echo base_url('inc/images/logo.png');?>" alt="Rapido"/>
						</a>
						<!-- end: LOGO -->
					</div>
					<div class="topbar-tools">
						<!-- start: TOP NAVIGATION MENU -->
						<ul class="nav navbar-right">
							<!-- start: USER DROPDOWN -->
							<li class="dropdown current-user">
								<a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
									<img src="<?php echo base_url('inc/images/avatar-1-small.jpg');?>" class="img-circle" alt=""> <span class="username hidden-xs">Peter Clark</span> <i class="fa fa-caret-down "></i>
								</a>
								<ul class="dropdown-menu dropdown-dark">
									<li>
										<a href="pages_user_profile.html">
											My Profile
										</a>
									</li>
									<li>
										<a href="pages_calendar.html">
											My Calendar
										</a>
									</li>
									<li>
										<a href="pages_messages.html">
											My Messages (3)
										</a>
									</li>
									<li>
										<a href="login_lock_screen.html">
											Lock Screen
										</a>
									</li>
									<li>
										<a href="login_login.html">
											Log Out
										</a>
									</li>
								</ul>
							</li>
							<!-- end: USER DROPDOWN -->
							<li class="right-menu-toggle">
								<a href="#" class="sb-toggle-right">
									<i class="fa fa-globe toggle-icon"></i> <i class="fa fa-caret-right"></i> <span class="notifications-count badge badge-default hide"> 3</span>
								</a>
							</li>
						</ul>
						<!-- end: TOP NAVIGATION MENU -->
					</div>
				</div>
				<!-- end: TOPBAR CONTAINER -->
			</header>
			<!-- end: TOPBAR -->
			<!-- start: PAGESLIDE LEFT -->
			<a class="closedbar inner hidden-sm hidden-xs" href="#">
			</a>
			<nav id="pageslide-left" class="pageslide inner">
				<div class="navbar-content">
					<!-- start: SIDEBAR -->
					<div class="main-navigation left-wrapper transition-left">
						<div class="navigation-toggler hidden-sm hidden-xs">
							<a href="#main-navbar" class="sb-toggle-left">
							</a>
						</div>
						<div class="user-profile border-top padding-horizontal-10 block">
							<div class="inline-block">
								<img src="<?php echo base_url('inc/images/avatar-1.jpg');?>" alt="">
							</div>
							<div class="inline-block">
								<h5 class="no-margin"> Welcome </h5>
								<h4 class="no-margin"> Peter Clark </h4>
								<a class="btn user-options sb_toggle">
									<i class="fa fa-cog"></i>
								</a>
							</div>
						</div>
						<!-- start: MAIN NAVIGATION MENU -->
						<ul class="main-navigation-menu">
							<li <?php if (base_url()){ echo 'class="active open"'; }?>>
								<a href="<?php echo base_url();?>"><i id="home" class="fa fa-home"></i> <span class="title"> Dashboard </span></a>
							</li>
							<li>
								<a href="javascript:void(0)"><i class="fa fa-desktop"></i> <span class="title"> Pentadbiran </span><i class="icon-arrow"></i> </a>
								<ul class="sub-menu">
									<li>
										<a href="javascript:;">
											Pendaftaran
										</a>
									</li>
									<li>
										<a href="javascript:;">
											Aset <i class="icon-arrow"></i>
										</a>
										<ul class="sub-menu">
											<li>
												<a href="<?php echo $l_pahm;?>">
													Harta Modal
												</a>
											</li>
											<li>
												<a href="layouts_horizontal_menu_fixed.html">
													Inventori
												</a>
											</li>
											<li>
												<a href="layouts_horizontal_sidebar_menu.html">
													Aset Alih
												</a>
											</li>
											<li>
												<a href="layouts_horizontal_sidebar_menu.html">
													Senarai Aset
												</a>
											</li>
										</ul>
									</li>
									<li>
										<a href="javascript:;">
											Kakitangan <i class="icon-arrow"></i>
										</a>
										<ul class="sub-menu">
											<li>
												<a href="layouts_horizontal_menu.html">
													Cuti
												</a>
											</li>
											<li>
												<a href="layouts_horizontal_menu_fixed.html">
													Perakam Waktu
												</a>
											</li>
										</ul>
									</li>
									<li>
										<a href="javascript:;">
											Stok <i class="icon-arrow"></i>
										</a>
										<ul class="sub-menu">
											<li>
												<a href="layouts_horizontal_menu.html">
													Permohonan Stok
												</a>
											</li>
											<li>
												<a href="layouts_horizontal_menu_fixed.html">
													Peralatan Sukan
												</a>
											</li>
										</ul>
									</li>
								</ul>
							</li>
							<li>
								<a href="javascript:void(0)"><i class="fa fa-cogs"></i> <span class="title"> Sukma </span><i class="icon-arrow"></i> </a>
								<ul class="sub-menu">
									<li>
										<a href="ui_elements.html">
											<span class="title"> Borang Pendaftaran </span>
										</a>
									</li>
									<li>
										<a href="javascript:;">
											<span class="title"> Lampiran </span><i class="icon-arrow"></i>
										</a>
										<ul class="sub-menu">
											<li>
												<a href="layouts_horizontal_menu.html">
													Penyataan Bank
												</a>
											</li>
											<li>
												<a href="layouts_horizontal_menu_fixed.html">
													IIC
												</a>
											</li>
										</ul>
									</li>
									<li>
										<a href="ui_icons.html">
											<span class="title"> Tawaran Atlet </span>
										</a>
									</li>
								</ul>
							</li>
							<li>
								<a href="javascript:void(0)"><i class="fa fa-th-large"></i> <span class="title"> USPTN </span><i class="icon-arrow"></i> </a>
								<ul class="sub-menu">
									<li>
										<a href="javascript:;">
											<span class="title">Biodata</span><i class="icon-arrow"></i>
										</a>
										<ul class="sub-menu">
											<li>
												<a href="bio_pegawai.php">
													Pegawai
												</a>
											</li>
											<li>
												<a href="bio_jurulatih.php">
													Jurulatih
												</a>
											</li>
											<li>
												<a href="bio_atlet.php">
													Atlet
												</a>
											</li>
										</ul>
									</li>
									<li>
										<a href="table_responsive.html">
											<span class="title">Maklumat Pusat Latihan</span>
										</a>
									</li>
									<li>
										<a href="table_data.html">
											<span class="title">Kehadiran Atlet</span>
										</a>
									</li>
									<li>
										<a href="table_export.html">
											<span class="title">Maklumat Atlet</span>
										</a>
									</li>
									<li>
										<a href="javascript:;">
											<span class="title">Laporan</span><i class="icon-arrow"></i>
										</a>
										<ul class="sub-menu">
											<li>
												<a href="layouts_horizontal_menu.html">
													Bulanan Jurulatih
												</a>
											</li>
											<li>
												<a href="layouts_horizontal_menu_fixed.html">
													Aktiviti
												</a>
											</li>
										</ul>
									</li>
									<li>
										<a href="javascript:;">
											<span class="title">Borang</span><i class="icon-arrow"></i>
										</a>
										<ul class="sub-menu">
											<li>
												<a href="layouts_horizontal_menu.html">
													Permohonan Peralatan
												</a>
											</li>
											<li>
												<a href="layouts_horizontal_menu_fixed.html">
													Permohonan Aktiviti
												</a>
											</li>
										</ul>
									</li>
									<li>
										<a href="taqwim.php">
											<span class="title">Taqwin</span>
										</a>
									</li>
								</ul>
							</li>
							
						</ul>
						<!-- end: MAIN NAVIGATION MENU -->
					</div>
					<!-- end: SIDEBAR -->
				</div>
				<div class="slide-tools">
					<div class="col-xs-6 text-left no-padding">
						<a class="btn btn-sm status" href="#">
							Status <i class="fa fa-dot-circle-o text-green"></i> <span>Online</span>
						</a>
					</div>
					<div class="col-xs-6 text-right no-padding">
						<a class="btn btn-sm log-out text-right" href="login_login.html">
							<i class="fa fa-power-off"></i> Log Out
						</a>
					</div>
				</div>
			</nav>
			<!-- end: PAGESLIDE LEFT -->
			<!-- start: PAGESLIDE RIGHT -->
			<div id="pageslide-right" class="pageslide slide-fixed inner">
				<div class="right-wrapper">
					<div class="notifications">
						<div class="pageslide-title">
							You have 11 notifications
						</div>
						<ul class="pageslide-list">
							<li>
								<a href="javascript:void(0)">
									<span class="label label-primary"><i class="fa fa-user"></i></span> <span class="message"> New user registration</span> <span class="time"> 1 min</span>
								</a>
							</li>
							<li>
								<a href="javascript:void(0)">
									<span class="label label-success"><i class="fa fa-comment"></i></span> <span class="message"> New comment</span> <span class="time"> 7 min</span>
								</a>
							</li>
							<li>
								<a href="javascript:void(0)">
									<span class="label label-success"><i class="fa fa-comment"></i></span> <span class="message"> New comment</span> <span class="time"> 8 min</span>
								</a>
							</li>
							<li>
								<a href="javascript:void(0)">
									<span class="label label-success"><i class="fa fa-comment"></i></span> <span class="message"> New comment</span> <span class="time"> 16 min</span>
								</a>
							</li>
							<li>
								<a href="javascript:void(0)">
									<span class="label label-primary"><i class="fa fa-user"></i></span> <span class="message"> New user registration</span> <span class="time"> 36 min</span>
								</a>
							</li>
							<li>
								<a href="javascript:void(0)">
									<span class="label label-warning"><i class="fa fa-shopping-cart"></i></span> <span class="message"> 2 items sold</span> <span class="time"> 1 hour</span>
								</a>
							</li>
							<li class="warning">
								<a href="javascript:void(0)">
									<span class="label label-danger"><i class="fa fa-user"></i></span> <span class="message"> User deleted account</span> <span class="time"> 2 hour</span>
								</a>
							</li>
						</ul>
						<div class="view-all">
							<a href="javascript:void(0)">
								See all notifications <i class="fa fa-arrow-circle-o-right"></i>
							</a>
						</div>
					</div>
					<div class="hidden-xs" id="style_selector">
						<div id="style_selector_container">
							<div class="pageslide-title">
								Style Selector
							</div>
							<div class="box-title">
								Choose Your Layout Style
							</div>
							<div class="input-box">
								<div class="input">
									<select name="layout" class="form-control">
										<option value="default">Wide</option><option value="boxed">Boxed</option>
									</select>
								</div>
							</div>
							<div class="box-title">
								Choose Your Header Style
							</div>
							<div class="input-box">
								<div class="input">
									<select name="header" class="form-control">
										<option value="fixed">Fixed</option><option value="default">Default</option>
									</select>
								</div>
							</div>
							<div class="box-title">
								Choose Your Sidebar Style
							</div>
							<div class="input-box">
								<div class="input">
									<select name="sidebar" class="form-control">
										<option value="fixed">Fixed</option><option value="default">Default</option>
									</select>
								</div>
							</div>
							<div class="box-title">
								Choose Your Footer Style
							</div>
							<div class="input-box">
								<div class="input">
									<select name="footer" class="form-control">
										<option value="default">Default</option><option value="fixed">Fixed</option>
									</select>
								</div>
							</div>
							<div class="box-title">
								10 Predefined Color Schemes
							</div>
							<div class="images icons-color">
								<a href="#" id="default"><img src="<?php echo base_url('inc/images/color-1.png');?>" alt="" class="active"></a>
								<a href="#" id="style2"><img src="<?php echo base_url('inc/images/color-2.png');?>" alt=""></a>
								<a href="#" id="style3"><img src="<?php echo base_url('inc/images/color-3.png');?>" alt=""></a>
								<a href="#" id="style4"><img src="<?php echo base_url('inc/images/color-4.png');?>" alt=""></a>
								<a href="#" id="style5"><img src="<?php echo base_url('inc/images/color-5.png');?>" alt=""></a>
								<a href="#" id="style6"><img src="<?php echo base_url('inc/images/color-6.png');?>" alt=""></a>
								<a href="#" id="style7"><img src="<?php echo base_url('inc/images/color-7.png');?>" alt=""></a>
								<a href="#" id="style8"><img src="<?php echo base_url('inc/images/color-8.png');?>" alt=""></a>
								<a href="#" id="style9"><img src="<?php echo base_url('inc/images/color-9.png');?>" alt=""></a>
								<a href="#" id="style10"><img src="<?php echo base_url('inc/images/color-10.png');?>" alt=""></a>
							</div>
							<div class="box-title">
								Backgrounds for Boxed Version
							</div>
							<div class="images boxed-patterns">
								<a href="#" id="bg_style_1"><img src="<?php echo base_url('inc/images/bg.png');?>" alt=""></a>
								<a href="#" id="bg_style_2"><img src="<?php echo base_url('inc/images/bg_2.png');?>" alt=""></a>
								<a href="#" id="bg_style_3"><img src="<?php echo base_url('inc/images/bg_3.png');?>" alt=""></a>
								<a href="#" id="bg_style_4"><img src="<?php echo base_url('inc/images/bg_4.png');?>" alt=""></a>
								<a href="#" id="bg_style_5"><img src="<?php echo base_url('inc/images/bg_5.png');?>" alt=""></a>
							</div>
							<div class="style-options">
								<a href="#" class="clear_style">
									Clear Styles
								</a>
								<a href="#" class="save_style">
									Save Styles
								</a>
							</div>
						</div>
						<div class="style-toggle open"></div>
					</div>
				</div>
			</div>
			<!-- end: PAGESLIDE RIGHT -->
			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					<!-- start: PANEL CONFIGURATION MODAL FORM -->
					<div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title">Panel Configuration</h4>
								</div>
								<div class="modal-body">
									Here will be a configuration form
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">
										Close
									</button>
									<button type="button" class="btn btn-primary">
										Save changes
									</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->